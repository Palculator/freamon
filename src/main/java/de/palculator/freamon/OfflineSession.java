package de.palculator.freamon;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import net.sf.json.JSONObject;

import org.apache.commons.lang.WordUtils;

import de.palculator.freamon.hal.FreamonHal;
import de.palculator.freamon.hal.SerializedFreamonHalTools;

/**
 * The OfflineSession class provides functionality to have multiple
 * {@link FreamonHal} instances talk amongst themselves and have their output
 * printed neatly formatted into a specified file.
 * 
 * Conversation participat
 * 
 * @author Marc Müller
 */
public class OfflineSession {
	
	private final Map<String, FreamonHal> participants = new HashMap<String, FreamonHal>();
	
	private final int limit;
	private final File out;
	
	public OfflineSession(JSONObject json) throws ClassNotFoundException, IOException {
		if (json.has("limit")) {
			this.limit = json.getInt("limit");
			if (this.limit < 0) {
				throw new IllegalArgumentException("Can't have a negative limit: " + this.limit);
			}
		}
		else {
			this.limit = Integer.MIN_VALUE;
		}
		
		if (json.has("file")) {
			this.out = new File(json.getString("file"));
		}
		else {
			this.out = null;
		}
		
		if (json.has("participants")) {
			this.initializeParticipants(json.getJSONObject("participants"));
		}
	}
	
	private void initializeParticipants(JSONObject jsonParticipants) throws ClassNotFoundException, IOException {
		for (Object key : jsonParticipants.keySet()) {
			String name = (String) key;
			File brainFile = new File(jsonParticipants.getString(name));
			FreamonHal hal = SerializedFreamonHalTools.read(brainFile);
			participants.put(name, hal);
		}
	}
	
	private int findLongestNameLength() {
		int max = Integer.MIN_VALUE;
		for (String name : this.participants.keySet()) {
			if (name.length() > max) {
				max = name.length();
			}
		}
		return max;
	}
	
	private String pickRandomTalker(String lastTalker) {
		Set<String> names = new HashSet<String>(this.participants.keySet());
		if (lastTalker != null && names.contains(lastTalker)) {
			names.remove(lastTalker);
		}
		int index = Configuration.RNG.nextInt(names.size());
		Iterator<String> iter = names.iterator();
		for (; index > 1; index--) {
			iter.next();
		}
		return iter.next();
	}
	
	private void printWrappedPhrase(StringBuilder sb, String line, int talkerTokenWidth) {
		String wrapped = WordUtils.wrap(line, 80 - talkerTokenWidth);
		String[] wrappedLines = wrapped.split(System.lineSeparator());
		if (wrappedLines.length > 0) {
			sb.append(wrappedLines[0]);
			sb.append(System.lineSeparator());
			for (int i = 1; i < wrappedLines.length; i++) {
				for (int j = 0; j < talkerTokenWidth; j++) {
					sb.append(' ');
				}
				sb.append(wrappedLines[i]);
				sb.append(System.lineSeparator());
			}
		}
	}
	
	public void converse() throws IOException {
		int count = 0;
		int maxNameWidth = this.findLongestNameLength() + 1;
		int talkerTokenwidth = maxNameWidth + 2;
		String lastTalker = null;
		String lastPhrase = null;
		StringBuilder nextLine = new StringBuilder();
		FileWriter file = null;
		if (this.out != null) {
			file = new FileWriter(this.out);
		}
		try {
			while (this.limit == Integer.MIN_VALUE || count < this.limit) {
				lastTalker = this.pickRandomTalker(lastTalker);
				int spaces = maxNameWidth - lastTalker.length();
				for (; spaces > 0; spaces--) {
					nextLine.append(' ');
				}
				nextLine.append(lastTalker);
				nextLine.append(": ");
				if (lastPhrase == null) {
					lastPhrase = this.participants.get(lastTalker).generateOriginalMessage();
				}
				else {
					lastPhrase = this.participants.get(lastTalker).instantMessage(lastPhrase);
				}
				this.printWrappedPhrase(nextLine, lastPhrase, talkerTokenwidth);
				String line = nextLine.toString();
				nextLine = new StringBuilder();
				if (file != null) {
					file.write(line + "\n");
				}
				if (this.limit != Integer.MIN_VALUE) {
					count++;
				}
			}
		}
		finally {
			if (file != null) {
				file.close();
			}
		}
	}
}
